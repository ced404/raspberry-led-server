// HTTP server
// var http = require("http").createServer(handler); //require http server, and create server with function handler()

// Express and http server
var http = require("http");
var express = require("express");

var app = (module.exports.app = express());
app.use(express.static("public"));

var server = http.createServer(app);
var io = require("socket.io").listen(server); // pass a http.Server instance
server.listen(8080);

var Gpio = require("onoff").Gpio;

// Read the LED state
var LED = new Gpio(4, "out"); // use GPIO pin 4 as output
var lightvalue = LED.readSync();

// Sleep function
const sleep = howLong => {
	return new Promise(resolve => {
		setTimeout(resolve, howLong);
	});
};

// Watch the LED state
const ledWatcher = async socket => {
	while (true) {
		if (lightvalue != LED.readSync()) {
			lightvalue = LED.readSync();
			io.sockets.emit("light", lightvalue);
			console.log("LED state changed on server (" + lightvalue + ")");
		}
		await sleep(100);
	}
};

// WebSocket Connection
io.sockets.on("connection", function(socket) {
	console.log("Sockets connected, LED is " + lightvalue);

	// NOTE: The "socket" object is related to a single browser connection.
	// The "io.sockets" object relates to all connected clients.
	if (lightvalue != LED.readSync()) {
		io.sockets.emit("light", lightvalue); // broadcat the LED initial state
	}

	socket.on("light", function(data) {
		lightvalue = data;
		if (lightvalue != LED.readSync()) {
			// only change LED if status has changed
			console.log("LED is " + lightvalue);
			io.sockets.emit("light", lightvalue); // emit LED state
			LED.writeSync(lightvalue); // update LED state
		}
	});

	// monitor the LED state
	ledWatcher(socket);
});

// Gracefully quit
process.on("SIGINT", function() {
	//on ctrl+c
	LED.writeSync(0); // Turn LED off
	LED.unexport(); // Unexport LED GPIO to free resources
	process.exit(); //exit completely
});
